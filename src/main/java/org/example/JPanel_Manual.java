package org.example;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JPanel_Manual extends JPanel {

    /* Component */

    private Label label_title;
    private TextArea text_area_description;
    private Label label_map;
    private JButton button_start;
    private JButton button_back;

    public JPanel_Manual() {

        // Set the layout of the panel

        setLayout(new GridBagLayout());
        Border border = BorderFactory.createLineBorder(Color.WHITE);
        setBorder(border);
        setBackground(Color.WHITE);

        /* Component assignment */

        // Label title

        label_title = new Label(new ImageIcon("img\\title.png"));

        // Text area description

        text_area_description = new TextArea(
                "In this mode you will command the drone.\nFind the cup and save the day.");
        text_area_description.setPreferredSize(new Dimension(250, 100));

        // Label map

        label_map = new Label(new ImageIcon("img\\map.png"));
        label_map.setPreferredSize(new Dimension(240, 220));
        Border border_map = BorderFactory.createTitledBorder(new LineBorder(Color.BLACK), "MAP");
        label_map.setBorder(border_map);

        // Start button

        button_start = new JButton(new ImageIcon("img\\START black.png"));
        button_start.setPreferredSize(new Dimension(250, 27));

        /* Page layout */

        // GBC

        GridBagConstraints gbc = new GridBagConstraints();

        // Button start

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(100, 0, 10, 0);

        add(button_start, gbc);

        // Start button to reach the game window
        button_start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button_start) {
                    JPanel_Manual_Game manualGamePanel = new JPanel_Manual_Game();
                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(JPanel_Manual.this);
                    frame.getContentPane().removeAll();
                    frame.getContentPane().add(manualGamePanel);
                    frame.revalidate();
                    frame.repaint();
                }
            }
        });

        // Label title

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 10;
        gbc.insets = new Insets(0, 0, 50, 0);

        add(label_title, gbc);

        // Text area description

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.insets = new Insets(0, 0, 70, 0);

        add(text_area_description, gbc);

        // Map

        gbc.gridx = 10;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 100, 60, 0);

        add(label_map, gbc);

        // Button back

        button_back = new JButton("BACK");
        button_back.setPreferredSize(new Dimension(250, 27));

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.insets = new Insets(10, 0, 0, 0);

        add(button_back, gbc);

        // Start button to reach the home window
        button_back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button_back) {
                    JPanel_Home homePanel = new JPanel_Home();
                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(JPanel_Manual.this);
                    frame.getContentPane().removeAll();
                    frame.getContentPane().add(homePanel);
                    frame.revalidate();
                    frame.repaint();
                }
            }
        });

    }

}
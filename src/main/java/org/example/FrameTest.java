package org.example;

import javax.swing.JFrame;
import java.util.concurrent.TimeUnit;

public class FrameTest extends JFrame{

    int[][] maze;

    int cellSize = 22;
    int personX = 1;
    int personY = 1;
    int cupX = 14; // Coordinate x della coppa
    int cupY = 17; // Coordinate y della coppa

    public FrameTest(int [][] maze){
        super("Maze");
        this.maze = maze;
        MazePanel mazePanel = new MazePanel(maze, cellSize, personX, personY, cupX, cupY);
        add(mazePanel);
        pack();
        Thread moveThread = new Thread(() -> {

        });
        moveThread.start();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

}


package org.example;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class JPanel_Win extends JPanel {

    /* Component */

    private Label label_title;
    private Label label_cup;
    private Label label_battery;
    private Label label_final;
    private JTextField field_battery;
    private TextArea textarea_text;

    public JPanel_Win(){

        // Set the layout of the panel

        setLayout(new GridBagLayout());
        Border border = BorderFactory.createEmptyBorder(10,20,10,20);
        setBorder(border);
        setBackground(Color.WHITE);

        /* Component assignment */



        // Label title

        label_title = new Label(new ImageIcon("img\\YOU WON black.png"));
        //label_title.setFont(new Font(TOOL_TIP_TEXT_KEY, Font.PLAIN, 34));



        // Text area

        textarea_text = new TextArea("The drone has successfully brought the cup");
        textarea_text.setPreferredSize(new Dimension(250,40));



        // Label final

        label_final = new Label(new ImageIcon("img\\THANKS black.png"));



        // Label cup

        label_cup = new Label(new ImageIcon("img\\cup.png"));



        // label battery

        label_battery = new Label(new ImageIcon("img\\battery.png"));



        // Field battery

        field_battery = new JTextField(7);
        field_battery.setEditable(true);
        field_battery.setPreferredSize(new Dimension(WIDTH, 22));



        // GBC

        GridBagConstraints gbc = new GridBagConstraints();



        // Label title

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.insets = new Insets(0, 0, 70, 0);

        add(label_title, gbc);



        // Text area

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(40, 0, 0, 0);

        add(textarea_text, gbc);



        // Label cup

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.5;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 25, 0);

        add(label_cup, gbc);



        // Label battery

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 0.1;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 100);

        add(label_battery, gbc);



        // field battery

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 0.1;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 100, 0, 0);

        add(field_battery, gbc);



        // Label final

        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(13, 0, 0, 0);

        add(label_final, gbc);


    }


}
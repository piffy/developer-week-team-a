package org.example;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;

public class Movement {

    private String Team;
    private String Seed;
    private int Width;
    private int Height;
    private int Posx;
    private int Posy;
    private int Energy;

    private int[] neighbors;

    private String inventory;

    private String status;


    public Movement(){
        this.Team="Aircraft";
        this.Seed="";
    }

    public Data INIT(String seed){
        String json;

        if(seed.compareTo("")!=0){
            json = "{\n\t\"team\": \"Aircraft\",\n\t\"seed\": \"" + seed + "\"\n}";

        } else {
            json = "{\n\t\"team\": \"Aircraft\"\n}";
        }
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://dw.gnet.it/init"))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .build();

        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            response.statusCode();
            String jsonResponse = response.body();
            // Parsing JSON and extracting data
            JSONObject jsonObject = new JSONObject(jsonResponse);
            setSeed(jsonObject.getString("seed"));
            setEnergy(jsonObject.getInt("Energy"));
            setPosx(jsonObject.getInt("posx"));
            setPosy(jsonObject.getInt("posy"));
            setWidth(jsonObject.getInt("width"));
            setHeight(jsonObject.getInt("height"));
            setTeam(jsonObject.getString("team"));
            // Creating and returning InitData instance
            return new Data(getSeed(), getEnergy(), getPosx(), getPosy(), getWidth(), getHeight(), getTeam());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public Data LOOK(String seed) {
        System.out.println(getTeam());
        String json = "{\n\t\"team\": \"" + getTeam() + "\",\n\t\"seed\": \"" + seed + "\"\n}";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://dw.gnet.it/look"))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .build();

        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            response.statusCode();
            String jsonResponse = response.body();

            // Parsing JSON and extracting data
            System.out.println(jsonResponse);
            JSONObject jsonObject = new JSONObject(jsonResponse);
            setPosx(jsonObject.getInt("posx"));
            setPosy(jsonObject.getInt("posy"));
            setEnergy(jsonObject.getInt("Energy"));

            JSONArray neighborsArray = jsonObject.getJSONArray("neighbors");
            int[] neighbors = new int[neighborsArray.length()];
            for (int i = 0; i < neighborsArray.length(); i++) {
                neighbors[i] = neighborsArray.getInt(i);
            }
            System.out.println(neighbors);

            setNeighbors(neighbors);

            // Creating and returning Data instance
            return new Data(getNeighbors(), getPosx(), getPosy(),getEnergy());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public Data MOVE(String seed, int pos) {
        String json = "{\n\t\"team\": \"" + getTeam() + "\",\n\t\"seed\": \"" + seed + "\",\n\t\"move\": " + pos + "\n}";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://dw.gnet.it/move"))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .build();

        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            String jsonResponse = response.body();


            // Controlla se la risposta contiene "illegal move"
            if (jsonResponse.contains("illegal move")) {
                // Se la mossa è illegale, ritorna semplicemente lo stato attuale senza aggiornamenti
                return new Data(getPosx(), getPosy(), getEnergy());
            }

            // Parsing JSON and extracting data
            JSONObject jsonObject = new JSONObject(jsonResponse);
            setPosx(jsonObject.getInt("posx"));
            setPosy(jsonObject.getInt("posy"));
            setEnergy(jsonObject.getInt("Energy"));

            // Creating and returning Data instance with updated position and energy
            return new Data(getPosx(), getPosy(), getEnergy());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Data LOAD(String seed) {
        String json = "{\n\t\"team\": \"" + getTeam() + "\",\n\t\"seed\": \"" + seed + "\"\n}";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://dw.gnet.it/load"))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .build();

        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            response.statusCode();
            String jsonResponse = response.body();

            // Controlla se il JSON inizia con '{' per determinare se è un oggetto JSON
            if (jsonResponse.startsWith("{")) {
                // Parsing JSON and extracting data
                JSONObject jsonObject = new JSONObject(jsonResponse);

                // Verifica se l'inventario contiene "goblet"
                if (jsonObject.has("inventory")) {
                    setInventory("goblet");
                }

                setEnergy(jsonObject.getInt("Energy"));
            } else {
                // Se la risposta non è un JSON, imposta l'inventario con la risposta come stringa
                setInventory(jsonResponse);

                if (response.body().compareTo("goblet is not here") != 0){
                    setEnergy(0); // Imposta l'energia a 0 o a un valore predefinito
                }
            }

            // Creazione e restituzione dell'istanza di Data
            return new Data(getInventory(), getEnergy());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Data UNLOAD(String seed) {
        String json = "{\n\t\"team\": \"" + getTeam() + "\",\n\t\"seed\": \"" + seed + "\"\n}";

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create("https://dw.gnet.it/unload"))
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .header("Content-Type", "application/json")
                .build();

        try {
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            response.statusCode();
            String jsonResponse = response.body();

            System.out.println(jsonResponse);

            // Controlla se il JSON inizia con '{' per determinare se è un oggetto JSON
            if (jsonResponse.startsWith("{")) {
                // Parsing JSON and extracting data
                JSONObject jsonObject = new JSONObject(jsonResponse);

                // Verifica se l'inventario contiene "goblet"
                if (jsonObject.has("status") && jsonObject.getString("status").equals("WIN")) {
                    setStatus(jsonObject.getString("status"));
                }

                setEnergy(jsonObject.getInt("Energy"));
            } else {
                // Se la risposta non è un JSON, imposta l'inventario con la risposta come stringa
                setStatus(jsonResponse);
                setEnergy(0); // Imposta l'energia a 0 o a un valore predefinito

            }

            // Creazione e restituzione dell'istanza di Data
            return new Data(getInventory(), getEnergy());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }





    public String getTeam() {
        return Team;
    }

    public void setTeam(String team) {
        Team = team;
    }

    public String getSeed() {
        return Seed;
    }

    public void setSeed(String seed) {
        Seed = seed;
    }

    public int getWidth() {
        return Width;
    }

    public void setWidth(int width) {
        Width = width;
    }

    public int getHeight() {
        return Height;
    }

    public void setHeight(int height) {
        Height = height;
    }

    public int getPosx() {
        return Posx;
    }

    public void setPosx(int posx) {
        Posx = posx;
    }

    public int getPosy() {
        return Posy;
    }

    public void setPosy(int posy) {
        Posy = posy;
    }

    public int getEnergy() {
        return Energy;
    }

    public void setEnergy(int energy) {
        Energy = energy;
    }

    public int[] getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(int[] neighbors) {
        this.neighbors = neighbors;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

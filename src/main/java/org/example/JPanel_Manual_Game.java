package org.example;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextField;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class JPanel_Manual_Game extends JPanel{

    private TextField text_location;
    private Label label_map;
    private Label icon_location;
    private TextField text_battery;
    private Label icon_battery;
    private TextField text_seed;
    private Label icon_seed;
    private Label icon_load;
    private JButton up;
    private JButton down;
    private JButton right;
    private JButton left;
    private JButton load;

    public JPanel_Manual_Game()
    {
        setLayout(new GridBagLayout());
        Border border = BorderFactory.createLineBorder(Color.WHITE);
        setBorder(border);

        GridBagConstraints gbc = new GridBagConstraints();

        /*Components */

        text_location = new TextField("location");
        text_location.setEditable(false);
        icon_location = new Label(new ImageIcon("img\\map_stats.png"));

        text_battery = new TextField("battery");
        text_battery.setEditable(false);
        icon_battery = new Label(new ImageIcon("img\\battery_stats.png"));

        text_seed = new TextField("seed");
        text_seed.setEditable(false);
        icon_seed = new Label(new ImageIcon("img\\sprout.png"));

        icon_load = new Label(new ImageIcon("img\\unloaded.png"));



        // Label map

        label_map = new Label(new ImageIcon("img\\map.png"));
        label_map.setPreferredSize(new Dimension(240, 220));



        /*Button */

        up = new JButton(new ImageIcon("img\\up.png"));
        up.setPreferredSize(new Dimension(65, 50));
        up.setBackground(Color.WHITE);

        down = new JButton(new ImageIcon("img\\down.png"));
        down.setPreferredSize(new Dimension(65, 50));
        down.setBackground(Color.WHITE);

        right = new JButton(new ImageIcon("img\\right.png"));
        right.setPreferredSize(new Dimension(65, 50));
        right.setBackground(Color.WHITE);

        left = new JButton(new ImageIcon("img\\left.png"));
        left.setPreferredSize(new Dimension(65, 50));
        left.setBackground(Color.white);

        load = new JButton(new ImageIcon("img\\load_unload.png"));
        load.setPreferredSize(new Dimension(65, 50));
        load.setBackground(Color.white);



        //Label icon unload

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,200,350,0);

        add(icon_load, gbc);



        //label icon location

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,350,300);

        add(icon_location, gbc);



        //label text location

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,350,160);

        add(text_location, gbc);



        //label icon seed

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,350,30);

        add(icon_seed, gbc);



        //label text seed

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,70,350,0);

        add(text_seed, gbc);



        //label icon battery

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,300,350,0);
        gbc.gridheight = 1;

        add(icon_battery, gbc);



        //label text battery

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,430,350,0);
        gbc.gridheight = 1;

        add(text_battery, gbc);



        //label UP arrow

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridheight = 0;
        gbc.gridwidth = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,300,520);

        add(up, gbc);



        //label DOWN arrow

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,150,520);

        add(down, gbc);



        //label RIGHT arrow

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,0,520);

        add(right, gbc);



        //label LEFT arrow

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(150,0,0,520);

        add(left, gbc);



        //label load

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.5;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(300,0,0,520);

        add(load, gbc);



        // Map

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(100, 100, 0, 0);

        add(label_map, gbc);
    }

}

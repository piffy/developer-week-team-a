package org.example;

import java.sql.Array;
import java.util.ArrayList;

public class Drone {
    private Movement movement;
    private int energy;

    private ArrayList <Cell> alreadySeenArray = new ArrayList <Cell>();
    private ArrayList <CrossRoad> crossRoadsArray = new ArrayList <CrossRoad>();

    public Drone() {
        this.movement = new Movement();
    }

    public void start(Maze maze) {
        System.out.println("Start simulation...");
        this.movement.setEnergy(maze.energy);
        while (this.movement.getEnergy() > 0) {
            // check if drone has the goblet
            if (hasGoblet(maze.seed)) {
                System.out.println("You won! You reached the goblet.");
                return;
            }

            // check if the drone has energy
            if (this.movement.getEnergy() <= 0) {
                System.out.println("No energy remaining! End simulation.");
                return;
            }
            Data lookData = movement.LOOK(maze.seed);
            maze.printMaze(lookData);
            // if the drone does not have the goblet and has energy, make a movement
            int movementSuggestion = this.getSuggestedDirection(lookData);
            if (movementSuggestion == -1) {
                System.out.println("Error during movement.");
                return;
            }
            Data moveData = this.movement.MOVE(maze.seed, movementSuggestion);
            if (moveData != null) {
                energy = moveData.getEnergy(); // Aggiorna l'energia residua dopo il movimento
                System.out.println("Energy remaining: " + energy);
            } else {
                System.out.println("Error during movement.");
                return;
            }
        }
    }

    public int getSuggestedDirection(Data data) {
        int moveDirection = -1;

        System.out.println(data);

        // is there a gobblet nearby?
        for (int i = 0; i < data.getNeighbors().length; i++) {
            if (data.getNeighbors()[i] == 3) {
                moveDirection = i*2;
                System.out.println("goblet found");
                return moveDirection;
            }
        }

        int currentX = data.getPosX();
        int currentY = data.getPosY();

        Cell currentCell = new Cell(currentX,currentY);
        if (!this.hasAlreadySeenCell(currentX, currentY)) {
            this.alreadySeenArray.add(currentCell);
        }
        int possiblePath = 0;



        // move in any new direction
        for (int i = 0; i < data.getNeighbors().length; i++) {
            if (data.getNeighbors()[i] == 0) {
                possiblePath++;
                if (i == 0 && moveDirection == -1) {
                    // up
                    Cell nextCell = new Cell(currentX, currentY - 1);
                    if (!this.hasAlreadySeenCell(nextCell.x, nextCell.y)) {
                        moveDirection = i;
                        System.out.println("up");
                    }
                } else if (i == 1 && moveDirection == -1) {
                    // right
                    Cell nextCell = new Cell(currentX + 1, currentY);
                    if (!this.hasAlreadySeenCell(nextCell.x, nextCell.y)) {
                        moveDirection = 2;

                        System.out.println("right");
                    }
                } else if (i == 2 && moveDirection == -1) {
                    // below
                    Cell nextCell = new Cell(currentX, currentY + 1);
                    if (!this.hasAlreadySeenCell(nextCell.x, nextCell.y)) {
                        moveDirection = 4;

                        System.out.println("down");
                    }
                } else if (i == 3 && moveDirection == -1) {
                    // left
                    Cell nextCell = new Cell(currentX -1, currentY);
                    if (!this.hasAlreadySeenCell(nextCell.x, nextCell.y)) {
                        moveDirection = 6;

                        System.out.println("left");
                    }
                }
            }
        }

        //check if latest CrossRoad is this one, if not create a new one


        if (moveDirection != -1 && !crossRoadsArray.isEmpty()){
            if (possiblePath > 1) {
                ArrayList<Cell> emptyArrayList = new ArrayList<Cell>();
                CrossRoad latestCrossRoad = new CrossRoad(currentX, currentY, emptyArrayList);
                this.crossRoadsArray.add(latestCrossRoad);
                this.crossRoadsArray.get(this.crossRoadsArray.size()-1).path.add((new Cell(currentX, currentY)));
                System.out.println("adding new crossroad");
            }else{
                this.crossRoadsArray.get(this.crossRoadsArray.size()-1).path.add((new Cell(currentX, currentY)));
                System.out.println("adding path to old crossroad");
            }
            return moveDirection;
        }

        // go back to previous free path
        try {
            Cell lastCellPath = this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.get(this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() - 1);
            if (lastCellPath.y < currentY) {
                // up
                moveDirection = 0;
                this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.remove(this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() - 1);
                if (this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() == 0) {
                    this.crossRoadsArray.remove(this.crossRoadsArray.size() - 1);
                }
                System.out.println("again up");

            } else if (lastCellPath.x > currentX) {
                // right
                moveDirection = 2;
                this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.remove(this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() - 1);
                if (this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() == 0) {
                    this.crossRoadsArray.remove(this.crossRoadsArray.size() - 1);
                }
                System.out.println("again right");
            } else if (lastCellPath.y > currentY) {
                // below
                moveDirection = 4;
                this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.remove(this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() - 1);
                if (this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() == 0) {
                    this.crossRoadsArray.remove(this.crossRoadsArray.size() - 1);
                }
                System.out.println("again down");
            } else if (lastCellPath.x < currentX) {
                // left
                moveDirection = 6;
                this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.remove(this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() - 1);
                if (this.crossRoadsArray.get(this.crossRoadsArray.size() - 1).path.size() == 0) {
                    this.crossRoadsArray.remove(this.crossRoadsArray.size() - 1);
                }
                System.out.println("again left");
            }
        } catch (Exception e){

        }
        return moveDirection;
    }

    private boolean hasGoblet(String seed) {
        Data loadData = this.movement.LOAD(seed);
        if (loadData != null && loadData.getInventory().compareTo("goblet") == 0) {
            return true;
        }
        return false;
    }

    private boolean hasAlreadySeenCell(int x, int y){
        for(int i = 0; i < this.alreadySeenArray.size(); i++){
            if (this.alreadySeenArray.get(i).x == x && this.alreadySeenArray.get(i).y == y){
                return true;
            }
        }return false;

    }
}
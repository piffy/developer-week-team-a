package org.example;

import java.awt.Dimension;

import javax.swing.JFrame;

public class Frame_Win extends JFrame {

    JPanel_Win win;

    public Frame_Win() {

        win = new JPanel_Win();
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setSize(new Dimension(600,500));
        setLocationRelativeTo(null);
        add(win);

    }

}


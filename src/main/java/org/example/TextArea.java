package org.example;


import javax.swing.*;
import java.awt.*;

public class TextArea extends JTextArea {

    public TextArea(String text) {

        super(text);

        setPreferredSize(new Dimension(300, 100));

        setEditable(false);

        setPreferredSize(new Dimension(250, 100));

    }

}

package org.example;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextField;

import javax.swing.*;
import javax.swing.border.Border;

public class JPanel_SemiAuto_Game extends JPanel{

    private Label label_map;
    private TextField text_location;
    private Label icon_location;
    private TextField text_battery;
    private Label icon_battery;
    private TextField text_seed;
    private Label icon_seed;
    private Label icon_load;
    private JButton advancement;

    public JPanel_SemiAuto_Game()
    {
        setLayout(new GridBagLayout());
        Border border = BorderFactory.createLineBorder(Color.WHITE);
        setBorder(border);

        GridBagConstraints gbc = new GridBagConstraints();

        /*Component */

        // Label map

        label_map = new Label(new ImageIcon("img\\map.png"));
        label_map.setPreferredSize(new Dimension(240, 220));



        text_location = new TextField("location");
        text_location.setEditable(false);
        icon_location = new Label(new ImageIcon("img\\map_stats.png"));

        text_battery = new TextField("battery");
        text_battery.setEditable(false);
        icon_battery = new Label(new ImageIcon("img\\battery_stats.png"));

        text_seed = new TextField("seed");
        text_seed.setEditable(false);
        icon_seed = new Label(new ImageIcon("img\\sprout.png"));

        icon_load = new Label(new ImageIcon("img\\loaded.png"));

        advancement = new JButton(new ImageIcon("img\\advancement.png"));
        advancement.setPreferredSize(new Dimension(85, 50));
        advancement.setBackground(Color.WHITE);



        //icon laod

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,150,350,0);

        add(icon_load, gbc);



        // Icon battery

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,250,350,0);
        gbc.gridheight = 0;

        add(icon_battery, gbc);



        // Text battery

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,420,350,30);
        gbc.gridheight = 0;

        add(text_battery, gbc);



        // Button advancement

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,200,500);
        gbc.gridheight = 0;

        add(advancement, gbc);



        // Icon location

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 130, 0, 680);

        add(icon_location, gbc);



        // Text location

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,0,400);

        add(text_location, gbc);



        // Icon seed

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(200,0,0,540);
        add(icon_seed, gbc);



        // Text seed

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(200,0,0,400);

        add(text_seed, gbc);



        // Map

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 200, 0, 0);

        add(label_map, gbc);
    }
}
package org.example;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    private JPanel_Home panel_home;

    private JPanel_Win win;
    private JPanel_SemiAuto_Game panel_semiauto_game;
    private JPanel_Automatic panel_automaic;

    public Frame() {

        // Widow characteristics

        super("Harry Potter and The Dome Drone");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setResizable(false);

        /* Component assignment */

        // Panel Home (home-page)

        win = new JPanel_Win();

        panel_home = new JPanel_Home();

        // Add component

        // add(win);
        // add(panel_automaic);
        add(panel_home);

        // Pack

        pack();

        // Set visible: ON

        setSize(new Dimension(700, 500));
        setLocationRelativeTo(null);
        setVisible(true);

    }

}

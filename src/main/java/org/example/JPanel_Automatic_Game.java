package org.example;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.TextField;

import javax.swing.*;
import javax.swing.border.Border;

public class JPanel_Automatic_Game extends JPanel {
    private TextField text_location;
    private Label label_map;
    private Label icon_location;
    private TextField text_battery;
    private Label icon_battery;
    private TextField text_seed;
    private Label icon_seed;
    private Label icon_load;

    public JPanel_Automatic_Game() {
        setLayout(new GridBagLayout());
        Border border = BorderFactory.createLineBorder(Color.WHITE);
        setBorder(border);

        GridBagConstraints gbc = new GridBagConstraints();

        // Label map

        label_map = new Label(new ImageIcon("img\\map.png"));
        label_map.setPreferredSize(new Dimension(240, 220));

        text_location = new TextField("location");
        text_location.setEditable(false);
        icon_location = new Label(new ImageIcon("img\\map_stats.png"));

        text_battery = new TextField("battery");
        text_battery.setEditable(false);
        icon_battery = new Label(new ImageIcon("img\\battery_stats.png"));

        text_seed = new TextField("seed");
        text_seed.setEditable(false);
        icon_seed = new Label(new ImageIcon("img\\sprout.png"));

        icon_load = new Label(new ImageIcon("img\\loaded.png"));


        //label icon load

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 120, 400, 0);

        add(icon_load, gbc);


        //label icon location

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 400, 350);

        add(icon_location, gbc);


        //label text location

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 400, 200);

        add(text_location, gbc);


        //label icon seed

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 400, 76);

        add(icon_seed, gbc);


        //label text seed

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 20, 400, 0);

        add(text_seed, gbc);


        //label icon battery

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 220, 400, 0);
        gbc.gridheight = 0;

        add(icon_battery, gbc);


        //label text battery

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 360, 400, 0);
        gbc.gridheight = 0;

        add(text_battery, gbc);


        // Map

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(label_map, gbc);
    }
}
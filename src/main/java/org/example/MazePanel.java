package org.example;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class MazePanel extends JPanel {
    private int[][] maze;
    private int[][] visible;
    private int cellSize;
    private int personX;
    private int personY;
    private int cupX;
    private int cupY;
    private int visited [][];

    public MazePanel(int[][] maze, int cellSize, int personX, int personY, int cupX, int cupY) {
        this.maze = maze;
        this.cellSize = cellSize;
        this.personX = personX;
        this.personY = personY;
        this.cupX = cupX;
        this.cupY = cupY;
        this.visited = new int[maze.length][maze[0].length];
        visited[personX][personY] = 1;
        int width = maze.length * cellSize;
        int height = maze[0].length * cellSize;
        setPreferredSize(new Dimension(width, height));

        visible = new int[maze.length][maze[0].length];
        updateVisibleCells();

        setFocusable(true);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_UP:
                        movePerson(0, -1);
                        break;
                    case KeyEvent.VK_DOWN:
                        movePerson(0, 1);
                        break;
                    case KeyEvent.VK_LEFT:
                        movePerson(-1, 0);
                        break;
                    case KeyEvent.VK_RIGHT:
                        movePerson(1, 0);
                        break;
                }
                checkWin();
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[0].length; j++) {
                if (visible[i][j] == 1) {
                    if (maze[i][j] == 1) {
                        g.setColor(Color.BLACK); // Wall
                        g.fillRect(i * cellSize, j * cellSize, cellSize, cellSize);
                    } else {
                        g.setColor(Color.WHITE); // Corridor
                        g.fillRect(i * cellSize, j * cellSize, cellSize, cellSize);
                    }
                } else {
                    g.setColor(Color.BLACK); // Hidden cells
                    g.fillRect(i * cellSize, j * cellSize, cellSize, cellSize);
                }
            }
        }

        // Draw person as red square
        g.setColor(Color.RED);
        g.fillRect(personX * cellSize + cellSize / 4 - 3, personY * cellSize + cellSize / 4 - 3, cellSize - 6, cellSize - 6);

        // Draw cup as blue circle if it's adjacent to the person
        if (visible[cupX][cupY] == 1) {
            g.setColor(Color.BLUE);
            int cupSize = cellSize / 2;
            g.fillOval(cupX * cellSize + cellSize / 4, cupY * cellSize + cellSize / 4, cupSize, cupSize);
        }
    }

    public void movePerson(int dx, int dy) {
        int newX = personX + dx;
        int newY = personY + dy;
        if (newX >= 0 && newX < maze.length && newY >= 0 && newY < maze[0].length && maze[newX][newY] == 0) {
            personX = newX;
            personY = newY;
            updateVisibleCells();
            repaint();
        }
    }

    public void moveLeft() {
        if (personX > 0 && maze[personX - 1][personY] == 0) {
            personX--;
            updateVisibleCells();
            repaint();
        }
    }

    public void moveRight() {
        if (personX < maze.length - 1 && maze[personX + 1][personY] == 0) {
            personX++;
            updateVisibleCells();
            repaint();
        }
    }

    public void moveUp() {
        if (personY > 0 && maze[personX][personY - 1] == 0) {
            personY--;
            updateVisibleCells();
            repaint();
        }
    }

    public void moveDown() {
        if (personY < maze[0].length - 1 && maze[personX][personY + 1] == 0) {
            personY++;
            updateVisibleCells();
            repaint();
        }
    }

    public void checkWin() {
        if (personX == cupX && personY == cupY) {
            new Frame_Win();
        }
    }

    private void updateVisibleCells() {
        // Inizializza tutte le celle come invisibili
        for (int i = 0; i < visible.length; i++) {
            for (int j = 0; j < visible[0].length; j++) {
                visible[i][j] = 0;
            }
        }

        // Rendi visibili il muro esterno
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[0].length; j++) {
                if (i == 0 || i == maze.length - 1 || j == 0 || j == maze[0].length - 1) {
                    visible[i][j] = 1;
                }
            }
        }

        // Rendi visibili le celle visitate
        for (int i = 0; i < visited.length; i++) {
            for (int j = 0; j < visited[0].length; j++) {
                if (visited[i][j] == 1) {
                    visible[i][j] = 1;
                }
            }
        }

        // Rendi visibili le celle adiacenti alla persona e segna le celle visitate
        for (int i = Math.max(0, personX - 1); i <= Math.min(maze.length - 1, personX + 1); i++) {
            for (int j = Math.max(0, personY - 1); j <= Math.min(maze[0].length - 1, personY + 1); j++) {
                if (maze[i][j] == 0) {
                    visible[i][j] = 1;
                    visited[i][j] = 1; // Segna la cella come visitata
                }
            }
        }
    }
}

package org.example;

public class Maze {
    private int[][] maze;
    public String seed;

    // Dimensioni della matrice del labirinto
    private int mazeWidth;
    private int mazeHeight;

    // Posizione iniziale del drone
    private int posStartX;
    private int posStartY;
    private int posDroneX;
    private int posDroneY;
    public int energy;
    public Maze() {

    }

    public void startPartita(){
        generateMaze();
    }

    private void generateMaze() {
        // Utilizza il comando INIT per ottenere le dimensioni e le informazioni sul labirinto
        Movement movement = new Movement();
        Data initData = movement.INIT("");
        System.out.println(initData);
        this.seed = initData.getSeed();

        mazeWidth = initData.getWidth();
        mazeHeight = initData.getHeight();

        // Inizializza la matrice del labirinto
        maze = new int[mazeHeight][mazeWidth];

        // Imposta la posizione iniziale del drone
        posStartX = initData.getPosX();
        posStartY = initData.getPosY();
        posDroneX = posStartX;
        posDroneY = posStartY;
        int energy = initData.getEnergy();
        this.energy = initData.getEnergy();

        // Creazione della struttura di supporto per le barriere esterne
        int supportWidth = mazeWidth + 2; // Aggiungi 2 per i bordi esterni
        int supportHeight = mazeHeight + 2; // Aggiungi 2 per i bordi esterni
        int[][] supportStructure = new int[supportHeight][supportWidth];

        // Inizializzazione dei bordi esterni a 1
        for (int i = 0; i < supportHeight; i++) {
            for (int j = 0; j < supportWidth; j++) {
                if (i == 0 || i == supportHeight - 1 || j == 0 || j == supportWidth - 1) {
                    supportStructure[i][j] = 1;
                }
            }
        }
    }

    public int[][] getMaze() {
        return maze;
    }

    public void printMazeStart(int direction) {
        // Stampa la matrice del labirinto con la struttura di supporto
        for (int i = 0; i < maze.length + 2; i++) {
            for (int j = 0; j < maze[0].length + 2; j++) {
                // Se la posizione appartiene alla struttura di supporto, stampa il valore corrispondente
                if (i == 0 || i == mazeHeight + 1 || j == 0 || j == mazeWidth + 1) {
                    System.out.print("1 ");
                } else {
                    // Altrimenti, se la posizione corrisponde alla posizione del drone, stampa il valore "4"
                    if (posStartX == j && posStartY == i) {
                        System.out.print("4 ");
                    } else {
                        // Altrimenti, stampa il valore della matrice del labirinto
                        System.out.print(maze[i - 1][j - 1] + " ");
                    }
                }
            }
            System.out.println();
        }
    }
    private void changePos(int increaseX, int increaseY){

        posDroneX += increaseX;
        posDroneY += increaseY;


    }

    public void printMaze(Data data) {
        int posYUp = data.getPosY() - 1;
        int posYDown = data.getPosY() + 1;
        int posXRight = data.getPosX() + 1;
        int posXLeft = data.getPosX() - 1;

        // Stampa la matrice del labirinto con la struttura di supporto
        for (int i = 0; i < maze.length + 2; i++) {
            for (int j = 0; j < maze[0].length + 2; j++) {
                // Se la posizione appartiene alla struttura di supporto, stampa il valore corrispondente
                if (i == 0 || i == mazeHeight + 1 || j == 0 || j == mazeWidth + 1) {
                    System.out.print("1 ");
                } else {
                    // Altrimenti, se la posizione corrisponde alla posizione del drone, stampa il valore "4"
                    if (j == data.getPosX() + 1 && i == data.getPosY() + 1) {
                        System.out.print("4 ");
                    } else if(j == posXRight + 1 && i == data.getPosY() + 1) {

                        System.out.print(data.getNeighbors()[1] + " ");

                    }else if(j == posXLeft + 1 && i == data.getPosY() + 1) {

                        System.out.print(data.getNeighbors()[3] + " ");

                    }else if(j == data.getPosX() + 1 && i == posYUp + 1) {

                        System.out.print(data.getNeighbors()[0] + " ");

                    }else if(j == data.getPosX() + 1 && i == posYDown + 1) {

                        System.out.print(data.getNeighbors()[2] + " ");

                    }else{
                            // Altrimenti, stampa il valore della matrice del labirinto
                            System.out.print(maze[i - 1][j - 1] + " ");
                        }
                    }
                }System.out.println();
            }

        }
    }

package org.example;

import java.util.ArrayList;

public class CrossRoad {
    int x;
    int y;
    ArrayList <Cell> path = new ArrayList<Cell>();

    public CrossRoad(int x, int y, ArrayList<Cell> path) {
        this.x = x;
        this.y = y;
        this.path = path;
    }
}

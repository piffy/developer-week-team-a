package org.example;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.*;

public class JPanel_Home extends JPanel {

    /* Component */

    private Label label_title;
    private TextArea text_area_description;
    private Label label_map;
    private JButton button_automatic;
    private JButton button_manual;
    private JButton button_semi_auto;
    private JTextField field_seed;
    private Label label_seed;

    public JPanel_Home() {

        // Set the layout of the panel

        setLayout(new GridBagLayout());
        Border border = BorderFactory.createEmptyBorder(10, 20, 10, 20);
        setBorder(border);
        setBackground(Color.WHITE);

        /* Component assignment */

        // Label title

        label_title = new Label(new ImageIcon("img\\title.png"));

        // Text area description

        text_area_description = new TextArea(
                "Welcome to the Triwizard Tournament.\n             Will you find the cup?");
        Border border_text_area = BorderFactory.createEmptyBorder(0, 30, 0, 0);
        text_area_description.setBorder(border_text_area);
        text_area_description.setPreferredSize(new Dimension(300, 100));

        // Label map

        label_map = new Label(new ImageIcon("img\\map.png"));
        label_map.setPreferredSize(new Dimension(270, 240));
        Border border_map = BorderFactory.createTitledBorder(new LineBorder(Color.BLACK), "MAP");
        label_map.setBorder(border_map);

        // Mode buttons

        button_automatic = new JButton(new ImageIcon("img\\auto.png"));
        button_automatic.setPreferredSize(new Dimension(250, 27));
        button_manual = new JButton(new ImageIcon("img\\manual.png"));
        button_manual.setPreferredSize(new Dimension(250, 27));
        button_semi_auto = new JButton(new ImageIcon("img\\semi.png"));
        button_semi_auto.setPreferredSize(new Dimension(250, 27));

        // Field seed

        field_seed = new JTextField(10);
        field_seed.setEditable(true);
        field_seed.setPreferredSize(new Dimension(WIDTH, 22));

        // Label seed

        label_seed = new Label("SEED");
        Border border_seed = BorderFactory.createEmptyBorder(0, 0, 0, 45);
        label_seed.setBorder(border_seed);

        /* Page layout */

        // GBC

        GridBagConstraints gbc = new GridBagConstraints();

        // Label title

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(label_title, gbc);

        // Text area description

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.1;
        gbc.weighty = 0.1;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridwidth = 10;
        gbc.gridheight = 10;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(text_area_description, gbc);

        // Map image

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(label_map, gbc);

        // Button automatic

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 0.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 30, 10, 0);

        add(button_automatic, gbc);

        // Start button to reach the automatic mode window
        button_automatic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button_automatic) {
                    JPanel_Automatic automaticPanel = new JPanel_Automatic();
                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(JPanel_Home.this);
                    frame.getContentPane().removeAll();
                    frame.getContentPane().add(automaticPanel);
                    frame.revalidate();
                    frame.repaint();
                    // dispose();
                }
            }
        });

        // Button manual

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 0.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 30, 10, 0);

        add(button_manual, gbc);

        // Start button to reach the manual mode window
        button_manual.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button_manual) {
                    JPanel_Manual manualPanel = new JPanel_Manual();
                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(JPanel_Home.this);
                    frame.getContentPane().removeAll();
                    frame.getContentPane().add(manualPanel);
                    frame.revalidate();
                    frame.repaint();
                    // dispose();
                }
            }
        });

        // Button semi-automatic

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 0.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 30, 10, 0);

        add(button_semi_auto, gbc);

        // Start button to reach the semi-auto mode window
        button_semi_auto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button_semi_auto) {
                    JPanel_SemiAuto semiautoPanel = new JPanel_SemiAuto();
                    JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(JPanel_Home.this);
                    frame.getContentPane().removeAll();
                    frame.getContentPane().add(semiautoPanel);
                    frame.revalidate();
                    frame.repaint();
                    // dispose();
                }
            }
        });

        // Field seed

        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 0, 20, 70);

        add(field_seed, gbc);

        // Label seed

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.gridwidth = 10;
        gbc.gridheight = 1;
        gbc.insets = new Insets(0, 0, 20, 70);

        add(label_seed, gbc);

    }

}
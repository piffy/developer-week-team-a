package org.example;

import javax.swing.*;

public class Label extends JLabel {
    public Label(String text) {
        super(text);
    }

    public Label(Icon image) {
        super(image);
    }
}

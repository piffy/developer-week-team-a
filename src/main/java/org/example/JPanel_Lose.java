package org.example;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public class JPanel_Lose extends JPanel{

    /* Component */

    private Label label_title;
    private Label label_x;
    private Label label_battery;
    private Label label_final;
    private JTextField field_battery;
    private TextArea textarea_text;

    public JPanel_Lose(){

        // Set the layout of the panel

        setLayout(new GridBagLayout());
        Border border = BorderFactory.createEmptyBorder(10,20,10,20);
        setBorder(border);
        setBackground(Color.WHITE);

        /* Component assignment */



        // Label title

        label_title = new Label(new ImageIcon("img\\YOU LOST black.png"));



        // Text area 1 description

        textarea_text = new TextArea("The drone has exhausted the energy");
        textarea_text.setEditable(false);
        textarea_text.setPreferredSize(new Dimension(225,40));



        // Label final

        label_final = new Label(new ImageIcon("img\\THANKS black.png"));



        // Label X

        label_x = new Label(new ImageIcon("img\\cross.PNG"));



        // Label Battery

        label_battery = new Label(new ImageIcon("img\\NoBattery.png"));



        // Field Battery

        field_battery = new JTextField(7);
        field_battery.setEditable(true);
        field_battery.setPreferredSize(new Dimension(WIDTH, 22));



        // GBC

        GridBagConstraints gbc = new GridBagConstraints();



        // Label title

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.insets = new Insets(0, 0, 70, 0);

        add(label_title, gbc);



        // Text area 1 description

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(textarea_text, gbc);



        // Label final

        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(label_final, gbc);

        // Label cross

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.weighty = 0.5;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 0);

        add(label_x, gbc);

        // label no-battery

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 0.1;
        gbc.weighty = 1.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 0, 0, 100);

        add(label_battery, gbc);

        // Label field

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 0.1;
        gbc.weighty = 0.0;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0, 100, 0, 0);

        add(field_battery, gbc);

    }

}
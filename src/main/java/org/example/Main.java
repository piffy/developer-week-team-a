package org.example;

public class Main {
    public static void main(String[] args) {

        int direction = -1;

        Maze maze = new Maze();

        maze.startPartita();

        maze.printMazeStart(direction);

        Drone drone=new Drone();

        drone.start(maze);

    }
}
package org.example;

import java.util.Arrays;

public class Data {
    private String seed;
    private int energy;
    private int posX;
    private int posY;
    private int width;
    private int height;
    private String team;
    private int[] neighbors; // Aggiunto per LOOK
    private String inventory;
    private String status;

    public Data(String seed, int energy, int posX, int posY, int width, int height, String team) {
        this.seed = seed;
        this.energy = energy;
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
        this.team = team;
    }

    public Data(int[] neighbors,int posX,int posY,int energy) {
        this.neighbors=neighbors;
        this.energy = energy;
        this.posX = posX;
        this.posY = posY;
    }

    public Data(int posX,int posY,int energy) {
        this.energy = energy;
        this.posX = posX;
        this.posY = posY;
    }

    public Data(String inventory,int energy) {
        this.inventory = inventory;
        this.energy = energy;
    }



    // Aggiunti metodi getter e setter per neighbors
    public int[] getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(int[] neighbors) {
        this.neighbors = neighbors;
    }

    // Aggiunti metodi getter e setter per energy
    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    // Aggiunti metodi getter e setter per width e height
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    // Aggiunti metodi getter e setter per team e seed
    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getSeed() {
        return seed;
    }

    public void setSeed(String seed) {
        this.seed = seed;
    }

    // Aggiunti metodi getter e setter per posX e posY
    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Data{" +
                "seed='" + seed + '\'' +
                ", energy=" + energy +
                ", posX=" + posX +
                ", posY=" + posY +
                ", width=" + width +
                ", height=" + height +
                ", team='" + team + '\'' +
                ", neighbors=" + Arrays.toString(neighbors) +
                ", inventory='" + inventory + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
